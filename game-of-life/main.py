import time

board = [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0],
    [0,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
]

TICK = 0.3
BOARD_Y = len(board)
BOARD_X = len(board[0])

def init_board():
    new_board = []
    for row_i in range(BOARD_Y):
        new_board.append([])
        for cell_i in range(BOARD_X):
            new_board[row_i].append(0)
    return new_board

def print_board(board):
    for row_i in range(BOARD_Y):
        for cell_i in range(BOARD_X):
            if board[row_i][cell_i]:
                print('●', end='')
            else:
                print('○', end='')
            print(end=' ')
        print()

    for _ in range(BOARD_X):
        print(end='__')
    print()

def count_neighbours(board, row_i, cell_i):
    neighbours = 0
    for r in range(row_i - 1, row_i + 2):
        for c in range(cell_i - 1, cell_i + 2):
            if r == row_i and c == cell_i:
                continue
            neighbours += board[r % BOARD_Y][c % BOARD_X]
    return neighbours

def get_cell(cell, neighbours):
    new_cell = cell
    if neighbours < 2 or neighbours > 3:
        cell = 0
    elif neighbours == 3:
        cell = 1

    return cell

def simulate(board):
    new_board = init_board()
    for row_i in range(len(board)):
        for cell_i in range(len(board[row_i])):
            new_board[row_i][cell_i] = get_cell(
                board[row_i][cell_i],
                count_neighbours(board, row_i, cell_i)
            )
    return new_board

while True:
    board = simulate(board)
    print_board(board)
    time.sleep(TICK)
